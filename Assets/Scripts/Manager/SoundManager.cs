using System;
using UnityEngine;

namespace Manager
{
    public class SoundManager : MonoBehaviour
    {
        [SerializeField] private SoundClip[] soundClips;
        [SerializeField] private AudioSource audioSource;
        
        public static SoundManager Instance { get; private set; }
        
        [Serializable]
        public struct SoundClip
        {
            public Sound Sound;
            public AudioClip AudioClip;
        }

        public enum Sound
        {
            BGM,
            PlayerDied,
            EnemyDied,
            
        }

        /// <summary>
        /// Play sound
        /// </summary>
        /// <param name="audioSource"></param>
        /// <param name="sound"></param>
        public void Play(AudioSource audioSource, Sound sound)
        {
            audioSource.clip = GetAudioClip(sound);
            audioSource.Play();
        }

        /// <summary>
        /// Play Background Music
        /// </summary>
        public void PlayBGM()
        {
            audioSource.loop = true;
            Play(audioSource, Sound.BGM);
        }

        private AudioClip GetAudioClip(Sound sound)
        {
            foreach (var soundClip in soundClips)
            {
                if (soundClip.Sound == sound)
                {
                    return soundClip.AudioClip;
                }
            }
            
            return null;
        }
        
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            
            DontDestroyOnLoad(this);
        }
    }
}
