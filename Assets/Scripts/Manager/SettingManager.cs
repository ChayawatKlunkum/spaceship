using UnityEngine;
using UnityEngine.SceneManagement;

namespace Manager
{
    public class SettingManager : MonoBehaviour
    {
        public void QuitGame()
        {
            Application.Quit();
            Debug.Log("Quit Game");
        }

        public void RestartSceneToLevel_1()
        {
            SceneManager.LoadScene("Level_1");
        }
        
        public void RestartSceneToLevel_2()
        {
            SceneManager.LoadScene("Level_2");
        }
    }
}
