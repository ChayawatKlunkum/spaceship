using UnityEngine;

namespace Spaceship
{
    public class Bullet : MonoBehaviour
    {
        [SerializeField] private int damage;
        [SerializeField] private float speed;
        [SerializeField] private new Rigidbody2D rigidbody2D;
        public void Init()
        {
            Move();
        }

        private void Move()
        {
            rigidbody2D.velocity = Vector2.up * speed;
        }
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            var target = other.gameObject.GetComponent<IDamagable>();
            target?.TakeHit(damage);
            Destroy(gameObject);
        }
    }
}