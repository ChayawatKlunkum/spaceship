using System;
using Manager;
using UnityEngine;

namespace Spaceship
{
    public class PlayerSpaceship : Basespaceship, IDamagable
    {
        public event Action OnExploded;

        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }

        public override void Fire()
        {
            var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
            bullet.Init();
        }

        public void TakeHit(int damage)
        {
            Hp -= damage;
            
            if (Hp > 0)
            {
                return;
            }
            
            Explode();
        }

        public void Explode()
        {
            SoundManager.Instance.Play(audioSource, SoundManager.Sound.PlayerDied);
            gameObject.SetActive(false);
            Destroy(gameObject);
            OnExploded?.Invoke();
        }
    }
}